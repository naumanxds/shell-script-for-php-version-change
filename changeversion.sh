echo -e "\e[1;31m This Script helps you change your php version on Ubuntu. \e[0m \n"

echo -e "\e[1;31m >> ============= List of Available PHP Versions ============ << \e[0m \n"

sudo update-alternatives --list php

echo -e "\n\e[1;31m >> ==================== Active PHP Version ================ << \e[0m \n"

php -v

echo -e "\n\e[1;31m >> ========================================================== << \e[0m \n"

echo $'\n\n'

read -p " Enter The Php Verison Number To DISABLE : " versionToDisable
read -p " Enter The Php Verison Number To ENABLE : " versionToEnable

sudo a2dismod php$versionToDisable

systemctl restart apache2

sudo a2enmod php$versionToEnable

sudo update-alternatives --set php /usr/bin/php$versionToEnable

sudo update-alternatives --set phar /usr/bin/phar$versionToEnable

sudo systemctl restart apache2

echo -e "\e[1;31m >>  All Done !!  << \e[0m\n\n"

echo -e "\n\e[1;31m >> ====================== New PHP Version =================== << \e[0m \n"

php -v

echo -e "\n\e[1;31m >> ========================================================== << \e[0m \n"